<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>栏目管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		
			
		<style type="text/css">
		.easyui-tabs table{
			width:450px;
			margin:10 10 10 10;
		}
		.easyui-tabs table tr .tit{
			width:150px;
			padding:2 5 2 5;
		} 
		.easyui-tabs table tr .val{
			width:200px;
			padding:2 5 2 5;
		} 
		.easyui-tabs table tr .val input{
			width:200px;
		} 
		.easyui-tabs table tr .val select{
			width:200px;
		} 
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.form.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/entity/node.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;">
	
	<div class="easyui-layout" fit="true">
   <div region="west" hide="true" split="false" fit="true" id="west">
	<div class="easyui-tabs"  fit="true" border="false" id="fm">
	<div title="基本信息">
    	<input type="hidden" mthod="smt" id="entityId" name="id" value="${entity.id }"/>
    	<input type="hidden" mthod="smt" id="leaf" name="leaf" value="${entity.leaf }"/>
    	<input type="hidden" mthod="smt" id="state" name="state" value="${entity.state}"/>
    	<input type="hidden" mthod="smt" id="createUser" name="createUser" value="${entity.createUser}"/>
    	<input type="hidden" mthod="smt" id="depth" name="depth" value="${entity.depth}"/>
    	<input type="hidden" mthod="smt" id="childCount" name="childCount" value="${entity.childCount}"/>
    	<input type="hidden" mthod="smt" id="countentCount" name="countentCount" value="${entity.countentCount}"/>
    	<input type="hidden" mthod="smt" id="isDeleted" name="isDeleted" value="${entity.isDeleted}"/>
    	<input type="hidden" mthod="smt" id="nodeIdPath" name="nodeIdPath" value="${entity.nodeIdPath}"/>
    	<input type="hidden" mthod="smt" id="subSiteId" name="subSiteId" value="${entity.subSiteId}"/>
    	<input type="hidden" mthod="smt" id="parentId" name="parentId" value="${entity.parentId}"/>
    	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${base }" var="field">
				
				<c:if test="${field.displayByEditMode==true }">
					<tr <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${entity[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${entity[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByEditMode==true }">
							<span class="int"><textarea mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>${entity[field.fieldName] }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
		</table>
    </div>
    <div title="展现设置">
    	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${view }" var="field">
				<c:if test="${field.displayByEditMode==true }">
					<tr <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${entity[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${entity[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByEditMode==true }">
							<span class="int"><textarea mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>${entity[field.fieldName] }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
		</table>
    </div>
    <div title="模板信息">
     	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${template }" var="field">
				<c:if test="${field.displayByEditMode==true }">
					<tr <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${entity[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${entity[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByEditMode==true }">
							<span class="int"><textarea mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>${entity[field.fieldName] }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==entity[field.fieldName] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
		</table>
    </div>
    </div>
    </div>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(function(){
	});
	String.prototype.replaceAll = function(s1,s2) { 
    	return this.replace(new RegExp(s1,"gm"),s2); 
	};
	function submit(){
		var _s_tmp=$("[mthod='smt']");
		var params="";
		for(i=0;i<_s_tmp.size();i++){
			var stit=_s_tmp[i].name;
			if(stit==null || stit==""){
				stit=_s_tmp[i].id;
			}
			if(_s_tmp[i].tagName=="SELECT"){
				params+=""+(stit)+":'"+$(_s_tmp[i]).val()+"',";
			}else if(_s_tmp[i].tagName=="INPUT"){
				if($(_s_tmp[i]).attr("data-options")+""!="undefined"){
					params+=""+(stit)+":'"+$(_s_tmp[i]).parent().find(".combo-value").val()+"',";
				}else{ 
					params+=""+(stit)+":'"+$(_s_tmp[i]).val()+"',";
				}
			
				
			}else{
			}
		} 
		params="[{"+params+"}]";
		
		$.post(
				path+"/Manage/Node/doEdit.do",
				eval(params)[0],
				function(result){
				//	alert(result);
					//rtn=true;
				}
			);
		return true;
	}
</script>