package com.ibpd.henuocms.tlds;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
/**
 * 循环tag 还可用，但一般 不用了
 * @author mg by qq:349070443
 *编辑于 2015-6-20 下午05:43:14
 */
public class ForeachTag extends SimpleTagSupport {
    // 标签属性
    private Object items;
    public Object getItems() {
		return items;
	}

	public void setItems(Object items) {
		this.items = items;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	private String var;
   //这里省略 getter setter方法

    @Override
    public void doTag() throws JspException, IOException {
        Iterator ite = null;
        
        Object tempItem = items;
        // 如果是集合
        if (tempItem instanceof Collection) {
            ite = ((Collection) tempItem).iterator();
        }
        // 如果是数组
        else if (tempItem instanceof Object[]) {
            ite = Arrays.asList((Object[]) tempItem).iterator();
        }
     // 如果是list
        else if (tempItem instanceof List) {
            ite = ((List)tempItem).iterator();
        }
     // 如果是null
        else if (tempItem==null) {
            ite = null;
            getJspContext().setAttribute(var, ite);
            if(getJspBody()!=null)
            	getJspBody().invoke(null);
            return ;
        }else {
            throw new RuntimeException("不能转换:"+items.getClass().getName());
        }
        // 进行迭代
        while (ite.hasNext()) {
            Object obj = ite.next();
            getJspContext().setAttribute(var, obj);
            //输出标签体
            if(getJspBody()!=null)
            	getJspBody().invoke(null);
        }
    }
}