package com.ibpd.henuocms.common;

import com.ibpd.shopping.entity.AccountEntity;


public class MailTemplateUtil {
	public static MailTemplateUtil getInterface(){
		return new MailTemplateUtil();
	}
	public String getMailContent(String type,Object o){
		//尚未完成
		if(o instanceof AccountEntity){
			AccountEntity a=(AccountEntity) o;
			if(type.toLowerCase().trim().equals("accountreg")){
				return "恭喜您:"+a.getAccount()+"<br/>于"+a.getRegeistDate().toLocaleString()+"注册成为手尚功夫会员,祝您购物愉快!";				
			}else if(type.toLowerCase().trim().equals("forgetpassword")){
				return "您的会员帐号:"+a.getAccount()+"<br/>执行了密码找回,新密码为:\""+a.getPassword()+"\"(不含双引号),请妥善保存.";				
			}
		}
		return "恭喜操作成功";
	}
}
