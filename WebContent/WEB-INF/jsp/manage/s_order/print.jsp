<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">   
<!--media=print 这个属性可以在打印时有效-->   
<style media=print>   
.Noprint{display:none;}   
.PageNext{page-break-after: always;}   
table{
}
table tr td{
	border:solid 1px black;
}
#orderInfo{
	width:180mm;
}
#orderInfo .header{
	height:30mm;
}
#orderInfo .header th span{
	font-size:10mm;
	text-align:center;
	overflow:hidden;
}
#orderInfo .orderBody{
}
#orderInfo .orderBody .k{
	text-align:left;
	padding-left:5mm;
	width:45mm;
	height:13mm;
}
#orderInfo .orderBody .v{
	text-align:center;
	width:45mm;
}
#detailInfo{
	margin-top:10mm;
	border:none;
	border-top:solid 1px black;
	width:100%;
}
#detailInfo .header{
	font-weight:bold;
	font-size:4mm;
	height:8mm;
}
#detailInfo .header .idx{
	width:10%;
	height:8mm;
	text-align:center;
	background-color:#464646;
}
#detailInfo .header .name{
	width:40%;
	text-align:center;
	background-color:#464646;
}
#detailInfo .header .num{
	width:10%;
	text-align:center;
	background-color:#464646;
}
#detailInfo .header .price{
	width:20%;
	text-align:center;
	background-color:#464646;
}
#detailInfo .header .total{
	width:20%;
	text-align:center;
	background-color:#464646;
}
#detailInfo .body{
}
#detailInfo .body .idx{
}
#detailInfo .body .name{
}
#detailInfo .body .num{
}
#detailInfo .body .price{
}
#detailInfo .body .total{
}

</style>   
 
<style media=screen>   
body{
	height:auto;
	overflow-y:scroll;
}
 
 
</style>   
 
</head>   
 
<body >   
<center class="Noprint" >  
<p>提示:下面的打印相关按钮仅仅在IE浏览器模式下有效，其他浏览器请尝试使用快捷键[Ctrl+P]进行打印操作.</p>
<p>该页面为订单单据打印页面，默认为纵向A4纸宽度（180mm）,打印过程中会自动根据订单分页打印.</p>
<p>   
<OBJECT id=WebBrowser classid=CLSID:8856F961-340A-11D0-A96B-00C04FD705A2 height=0 width=0>   
</OBJECT>   
<input type=button value=打印 onclick=document.all.WebBrowser.ExecWB(6,1)>  
<a onclick="javascrīpt:window.print()" target="_self">打印</a> 
<input type=button value=直接打印 onclick=document.all.WebBrowser.ExecWB(6,6)>   
<input type=button value=页面设置 onclick=document.all.WebBrowser.ExecWB(8,1)>   
</p>   
<p> <input type=button value=打印预览 onclick=document.all.WebBrowser.ExecWB(7,1)>   
<br/>   
</p>   
<hr align="center" width="90%" size="1" noshade>   
</center>   

	<c:forEach items="${lst }" var="order">
 <table id="orderInfo" cellpadding="0" cellspacing="0" border="1">
	<tr class="header">
		<th colspan="4"><span>${orderPrintHeader}</span></th>
	</tr>
	<tr class="orderBody">
		<td class="k">订单号</td>
		<td class="v">${order.orderNumber}</td>
		<td class="k">订单总金额</td>
		<td class="v">${order.amount}</td>
	</tr>
	<tr class="orderBody">
		<td class="k">下单日期</td>
		<td class="v">${order.createdate} &nbsp;</td>
		<td class="k">配送费</td>
		<td class="v">${order.fee}</td>
	</tr>
	<tr class="orderBody">
		<td class="k">收货人</td>
		<td class="v" colspan="3">${order.orderShip.shipaddress}&nbsp;&nbsp;${order.orderShip.shipname} &nbsp;&nbsp;(收)</td>
	</tr>
	<tr>
		<td colspan="4">
		<table id="detailInfo" cellpadding="0" cellspacing="0">
			<tr class="header">
				<td class="idx">序号</td>
				<td class="name">商品名称</td>
				<td class="num">数量</td>
				<td class="price">单价</td>
				<td class="total">小计</td>
			</tr>
		<c:forEach items="${order.detailList }" var="detail" varStatus="status">
			<tr class="body">
				<td class="idx">${ status.index + 1}</td>
				<td class="name">${detail.productName}</td>
				<td class="num">${detail.number}</td>
				<td class="price">${detail.price}</td>
				<td class="total">${detail.total}</td>
			</tr>
		</c:forEach>
		</table>
		</td>
	</tr>
 </table>
 <div class="PageNext"></div>   
	</c:forEach> 

</body>   
</html>  