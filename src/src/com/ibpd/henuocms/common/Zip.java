package com.ibpd.henuocms.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zip {
    public static void zipFiles(File[] srcfile,File zipfile){
        byte[] buf=new byte[1024];
        try {
            //ZipOutputStream类：完成文件或文件夹的压缩
            ZipOutputStream out=new ZipOutputStream(new FileOutputStream(zipfile));
            
            for(int i=0;i<srcfile.length;i++){
                FileInputStream in=new FileInputStream(srcfile[i]);
                out.putNextEntry(new ZipEntry(srcfile[i].getName()));
                int len;
                while((len=in.read(buf))>0){
                    out.write(buf,0,len);
                }
                		
                out.closeEntry();
                in.close();
            }
            out.close();
            System.out.println("压缩完成.");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

	public static void zipFiles(List<File> pathList, File zipfile) {
		File[] f=new File[pathList.size()];
		for(Integer i=0;i<pathList.size();i++){
			f[i]=pathList.get(i);
		}
		zipFiles(f,zipfile);
	}
	public static void main(String[] args){
		String p1="h:\\1.doc";
		String p2="E:\\work\\20150415_BDD92C792062EFA22D1D5020D4434DDC_new.csv";
		String p3="d:\\shutdown.bat";
		String zip="d:\\aaa.zip";
		List<File> l=new ArrayList<File>();
		l.add(new File(p1));
		l.add(new File(p2));
		l.add(new File(p3));
		File zipFile=new File(zip);
		Zip.zipFiles(l, zipFile);
		System.out.println("zip complete");
	}
}
